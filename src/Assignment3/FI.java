package Assignment3;

@FunctionalInterface

interface Calci{
	void cube(int i);
	
	default void square(int i) {
		System.out.println(i*i);
	}
	
	default void add(int i,int j) {
		
		System.out.println(i+j);
	}
	
	static void sub(int i,int j) {
		System.out.println(i-j);
	}
	
	static void mul(int i,int j) {
		
		System.out.println(i*j);
	}
	
	static void div(int i , int j) {
		System.out.println(i/j);
	}
}


public class FI implements Calci {
	
	@Override
	public void cube(int i) {
		System.out.println(i*i*i);
	}
	
	
	public static void main(String[] args) {
		FI obj = new FI();
		
		obj.add(1, 2);
		obj.cube(12);
		obj.square(12);
		Calci.div(15, 3);
		Calci.mul(17, 2);
		Calci.sub(29, 7);
		
	}

}

